package com.msewa.outaone.metadata

interface AppConstants {
    companion object {

        val MARKET= "Market"
        val MEMBERSHIP= "Membership"
        val USER_TYPE= "UserType"
        val PAYMENT_TYPE = "Payment Type"
        val REQ_WEBSITE = "RequiredWebsite"
        val PRICE = "Price"
    }
}
