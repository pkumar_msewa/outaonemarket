package com.msewa.outaone.subscriptions


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton

import com.msewa.outaone.R
import com.msewa.outaone.metadata.AppConstants

/**
 * A simple [Fragment] subclass.
 */
class PaymentFragment : Fragment() {

    private lateinit var paymentType: String
   private lateinit var price: String
    lateinit var planPriceTV: TextView
    lateinit var planTypeTV: TextView
    private lateinit var payButton: AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null){
            paymentType = arguments!!.getString(AppConstants.PAYMENT_TYPE)!!
            price = arguments!!.getString(AppConstants.PRICE)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_payment, container, false)

        planTypeTV = view.findViewById(R.id.planTv)
        planPriceTV = view.findViewById(R.id.planPriceTv)
        payButton = view.findViewById(R.id.pay_bt)

        planTypeTV.text = paymentType
        planPriceTV.text = price

        payButton.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container,ThanksFragment())
                    .addToBackStack(null)
                    .commit()
        }

        return view;
    }

    companion object{
        val TAG = "PaymentFragment"

        fun newInstance(paymentType:String,price:String): PaymentFragment{

            val paymentFragment = PaymentFragment()
            val args = Bundle()
            args.putString(AppConstants.PAYMENT_TYPE,paymentType)
            args.putString(AppConstants.PRICE,price)
            paymentFragment.arguments = args

            return paymentFragment

        }
    }




}
