package com.msewa.outaone.subscriptions


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton

import com.msewa.outaone.R

/**
 * A simple [Fragment] subclass.
 */
class SubscriptionBenefitsFragment : Fragment() {

    private lateinit var subscriptionButton : AppCompatButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_subscription_benefits, container, false)

        subscriptionButton = view.findViewById(R.id.subscription_bt)

        subscriptionButton.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container,SubscriptionMarketPlansFragment())
                    .addToBackStack(null)
                    .commit()
        }

        return view
    }


}
