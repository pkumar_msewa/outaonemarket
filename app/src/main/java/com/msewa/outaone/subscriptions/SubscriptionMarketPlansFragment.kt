package com.msewa.outaone.subscriptions


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.msewa.outaone.R
import com.msewa.outaone.authentication.SignupFragment
import com.msewa.outaone.metadata.AppConstants
import kotlinx.android.synthetic.main.fragment_subscription_plans.*

/**
 * A simple [Fragment] subclass.
 */
class SubscriptionMarketPlansFragment : Fragment() {
    private lateinit var submitButton1: AppCompatButton
    private lateinit var submitButton2: AppCompatButton
    private lateinit var submitButton3: AppCompatButton
    private lateinit var submitButton4: AppCompatButton

    private lateinit var userType: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null){
            userType = arguments!!.getString(AppConstants.USER_TYPE)!!
        }
    }

    companion object{

        fun newInstance(userType:String): SubscriptionMarketPlansFragment {

            val subscriptionPlansFragment = SubscriptionMarketPlansFragment()
            val args = Bundle()
            args.putString(AppConstants.USER_TYPE,userType)
            subscriptionPlansFragment.arguments = args

            return subscriptionPlansFragment
        }

    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_subscription_market_plans, container, false)

        submitButton1 = view.findViewById(R.id.submit1Pay)!!
        submitButton2 = view.findViewById(R.id.submit2Pay)!!
        submitButton3 = view.findViewById(R.id.submit3Pay)!!
        submitButton4 = view.findViewById(R.id.submit4Pay)!!

        submitButton1.setOnClickListener {

            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container,PaymentFragment.newInstance("Basic","$25/pm"))
                    .addToBackStack(null)
                    .commit()
        }
        submitButton2.setOnClickListener {

            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container,PaymentFragment.newInstance("Standard","$50/pm"))
                    .addToBackStack(null)
                    .commit()
        }

        submitButton3.setOnClickListener {

            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container,PaymentFragment.newInstance("Premium","$100/pm"))
                    .addToBackStack(null)
                    .commit()
        }

        submitButton4.setOnClickListener {

            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container,PaymentFragment.newInstance("Ultra Premium","$300/pm"))
                    .addToBackStack(null)
                    .commit()
        }

        return view
    }


}
