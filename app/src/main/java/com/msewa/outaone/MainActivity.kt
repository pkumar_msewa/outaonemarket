package com.msewa.outaone

import android.os.Bundle

import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

import android.view.View
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ImageButton
import android.widget.ProgressBar

class MainActivity : AppCompatActivity() {

   private lateinit var toolbar:Toolbar
   private lateinit var ivBackBtn:ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        ivBackBtn = findViewById(R.id.ivBackBtn)
        setSupportActionBar(toolbar)


        ivBackBtn.setOnClickListener {
            this.onBackPressed()
        }

        supportFragmentManager.beginTransaction()
                .replace(R.id.view_container, OutaHomeFragment()).commit()

    }

    companion object {
        private val TAG = "MainActivity"
    }
}
