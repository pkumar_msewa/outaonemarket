package com.msewa.outaone


import android.os.Bundle

import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.msewa.outaone.metadata.ApiURL


/**
 * A simple [Fragment] subclass.
 */
class OutaHomeFragment : Fragment() {
    private var marketButton: Button? = null
    private var memberShipButton: Button? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_outa_home, container, false)

        marketButton = view.findViewById(R.id.market_button)
        memberShipButton = view.findViewById(R.id.membership_button)

        memberShipButton!!.setOnClickListener {
           /* activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container, MembershipInfoFragment())
                    .addToBackStack(null)
                    .commit()*/
            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container, WebviewFragment.newInstance(ApiURL.OUTAONE_MEMBERSHIP))
                    .addToBackStack(null)
                    .commit()
        }

        marketButton!!.setOnClickListener {
           /* activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container, MarketInfoFragment())
                    .addToBackStack(null)
                    .commit()*/
            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container,WebviewFragment.newInstance(ApiURL.OUTAONE_MARKET))
                    .addToBackStack(null)
                    .commit()
        }

        return view
    }

    companion object {
        val TAG = "OutaHomeFragment"
    }

}
