package com.msewa.outaone.authentication


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatButton
import com.msewa.outaone.R
import com.msewa.outaone.metadata.AppConstants

/**
 * A simple [Fragment] subclass.
 */
class SignupFragment : Fragment() {

    private lateinit var countinueButton: AppCompatButton
    private lateinit var userType: String
    private lateinit var logo :ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null) {
            userType = arguments!!.getString(AppConstants.USER_TYPE)!!

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_outone_signup, container, false)

        countinueButton = view.findViewById(R.id.countinue_bt)
        logo = view.findViewById(R.id.outaone_icon)

        if (userType.equals(AppConstants.MARKET)){
            logo.setImageResource(R.drawable.outaone_market)
        }

        countinueButton.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container, PasswordFragment.newInstance(userType))
                    .addToBackStack(null)
                    .commit()
        }

        return view
    }

    companion object {

        fun newInstance(userType: String): SignupFragment {

            val signupFragment = SignupFragment()
            val args = Bundle()
            args.putString(AppConstants.USER_TYPE, userType)
            signupFragment.arguments = args

            return signupFragment
        }

    }


}
