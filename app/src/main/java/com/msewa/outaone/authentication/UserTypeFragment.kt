package com.msewa.outaone.authentication


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatButton
import com.msewa.outaone.R
import com.msewa.outaone.bussiness.BussinessInfoFragment
import com.msewa.outaone.metadata.AppConstants
import kotlinx.android.synthetic.main.fragment_user_type.*

/**
 * A simple [Fragment] subclass.
 */
class UserTypeFragment : Fragment() {
    private lateinit var nextButton: AppCompatButton
    private lateinit var userType: String
    private lateinit var logo : ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null){
            userType = arguments!!.getString(AppConstants.USER_TYPE)!!
        }
    }

    companion object{

        fun newInstance(userType:String):UserTypeFragment{

            val userTypeFragment = UserTypeFragment()
            val args = Bundle()
            args.putString(AppConstants.USER_TYPE,userType)
            userTypeFragment.arguments = args

            return userTypeFragment
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_user_type, container, false)

        nextButton = view.findViewById(R.id.next_bt)
        logo = view.findViewById(R.id.outaone_icon)

        if (userType.equals(AppConstants.MARKET)){
            logo.setImageResource(R.drawable.outaone_market)
        }

        nextButton.setOnClickListener {


            if (rbSeller.isChecked()) {
                activity!!.supportFragmentManager.beginTransaction()
                        .replace(R.id.view_container, BussinessInfoFragment.newInstance(userType))
                        .addToBackStack(null)
                        .commit()

            } else if (rbBuyer.isChecked()) {
                activity!!.supportFragmentManager.beginTransaction()
                        .replace(R.id.view_container, BussinessInfoFragment.newInstance(userType))
                        .addToBackStack(null)
                        .commit()
            }

        }

        return view
    }


}
