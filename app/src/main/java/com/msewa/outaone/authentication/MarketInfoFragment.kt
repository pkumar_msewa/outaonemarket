package com.msewa.outaone.authentication


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton

import com.msewa.outaone.R
import com.msewa.outaone.WebviewFragment
import com.msewa.outaone.metadata.ApiURL
import com.msewa.outaone.metadata.AppConstants

/**
 * A simple [Fragment] subclass.
 */
class MarketInfoFragment : Fragment() {

    private lateinit var enterButton: AppCompatButton
    private lateinit var subscribeButton: AppCompatButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_market_info, container, false)

        enterButton = view.findViewById(R.id.enter_bt)
        subscribeButton = view.findViewById(R.id.subscribe_bt)

        enterButton.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container,WebviewFragment.newInstance(ApiURL.OUTAONE_MARKET))
                    .addToBackStack(null)
                    .commit()
        }

        subscribeButton.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container,SignupFragment.newInstance(AppConstants.MARKET))
                    .addToBackStack(null)
                    .commit()
        }

        return view

    }


}
