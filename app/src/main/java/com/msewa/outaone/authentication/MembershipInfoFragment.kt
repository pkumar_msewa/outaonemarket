package com.msewa.outaone.authentication


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.msewa.outaone.R
import com.msewa.outaone.WebviewFragment
import com.msewa.outaone.metadata.ApiURL
import com.msewa.outaone.metadata.AppConstants

/**
 * A simple [Fragment] subclass.
 */
class MembershipInfoFragment : Fragment() {

    private var enterButton: Button? = null
    private var signUpButton: Button? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_membership_info, container, false)

        enterButton = view.findViewById(R.id.enter_bt)
        signUpButton = view.findViewById(R.id.sign_up_bt)


        enterButton!!.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container, WebviewFragment.newInstance(ApiURL.OUTAONE_MEMBERSHIP))
                    .addToBackStack(null)
                    .commit()
        }

        signUpButton!!.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container, SignupFragment.newInstance(AppConstants.MEMBERSHIP))
                    .addToBackStack(null)
                    .commit()
        }



        return view
    }


}
