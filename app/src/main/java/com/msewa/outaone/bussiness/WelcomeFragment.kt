package com.msewa.outaone.bussiness


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatButton

import com.msewa.outaone.R
import com.msewa.outaone.authentication.SignupFragment
import com.msewa.outaone.metadata.AppConstants
import com.msewa.outaone.subscriptions.SubscriptionBenefitsFragment
import com.msewa.outaone.subscriptions.SubscriptionPlansFragment

/**
 * A simple [Fragment] subclass.
 */
class WelcomeFragment : Fragment() {

    private lateinit var subscriptionButton: AppCompatButton
    private lateinit var userType: String
    private lateinit var logo :ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null){
            userType = arguments!!.getString(AppConstants.USER_TYPE)!!
        }
    }

    companion object{

        fun newInstance(userType:String): WelcomeFragment {

            val welcomeFragment = WelcomeFragment()
            val args = Bundle()
            args.putString(AppConstants.USER_TYPE,userType)
            welcomeFragment.arguments = args

            return welcomeFragment
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_welcome, container, false)

        subscriptionButton = view.findViewById(R.id.subscription_bt)
        logo = view.findViewById(R.id.outaone_icon)

        if (userType.equals(AppConstants.MARKET)){
            logo.setImageResource(R.drawable.outaone_market)
        }

        if (userType.equals(AppConstants.MEMBERSHIP)){
            subscriptionButton.setOnClickListener {
                activity!!.supportFragmentManager.beginTransaction()
                        .replace(R.id.view_container,SubscriptionPlansFragment.newInstance(userType))
                        .addToBackStack(null)
                        .commit()
            }
        }


        if (userType.equals(AppConstants.MARKET)){

            subscriptionButton.setOnClickListener {
                activity!!.supportFragmentManager.beginTransaction()
                        .replace(R.id.view_container,SubscriptionBenefitsFragment())
                        .addToBackStack(null)
                        .commit()
            }
        }

        return view;

    }




}
