package com.msewa.outaone.bussiness


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Spinner
import androidx.appcompat.widget.AppCompatButton

import com.msewa.outaone.R
import com.msewa.outaone.authentication.SignupFragment
import com.msewa.outaone.metadata.AppConstants

/**
 * A simple [Fragment] subclass.
 */
class BussinessDetailsFragment : Fragment() {
    private lateinit var nextButton: AppCompatButton
    private lateinit var spinner: Spinner
    private lateinit var userType: String
    private lateinit var logo : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null){
            userType = arguments!!.getString(AppConstants.USER_TYPE)!!
        }
    }

    companion object{

        fun newInstance(userType:String): BussinessDetailsFragment {

            val bussinessDetailsFragment = BussinessDetailsFragment()
            val args = Bundle()
            args.putString(AppConstants.USER_TYPE,userType)
            bussinessDetailsFragment.arguments = args

            return bussinessDetailsFragment
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_bussiness_details, container, false)

        nextButton = view.findViewById(R.id.next_bt)
        logo = view.findViewById(R.id.outaone_icon)

        if (userType.equals(AppConstants.MARKET)){
            logo.setImageResource(R.drawable.outaone_market)
        }

        nextButton.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.view_container,WelcomeFragment.newInstance(userType))
                    .addToBackStack(null)
                    .commit()
        }

        val bussinessType = arrayOf("Distributor", "Agent", "Hotel", "Supermarket/Shops", "Restaurants", "Institutions")
        spinner = view.findViewById(R.id.etTypeBussinessSpinner)

        val arrayAdapter = context?.let { ArrayAdapter(it,android.R.layout.simple_spinner_item, bussinessType) }
        arrayAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter=arrayAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //   Toast.makeText(this@BussinessIdentityActivity, getString(R.string.selected_item) + " " + bussinessType[position], Toast.LENGTH_SHORT).show()
            }

        }


        return view;
    }


}
