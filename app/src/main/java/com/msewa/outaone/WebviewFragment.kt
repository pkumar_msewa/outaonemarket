package com.msewa.outaone


import android.os.Bundle

import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.ProgressBar
import com.msewa.outaone.metadata.AppConstants


/**
 * A simple [Fragment] subclass.
 */
class WebviewFragment : Fragment() {

    private var webView: WebView? = null
    private var progressBar: ProgressBar? = null
    private lateinit var requiredWebsite: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null){
            requiredWebsite = arguments!!.getString(AppConstants.REQ_WEBSITE)!!
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_outa_market, container, false)

        webView = view.findViewById(R.id.web_view)
        progressBar = view.findViewById(R.id.progress_bar)

        webView!!.loadUrl(requiredWebsite)
        webView!!.settings.javaScriptEnabled = true

        webView!!.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                if (newProgress == 100) {
                    progressBar!!.visibility = View.GONE
                }
                super.onProgressChanged(view, newProgress)
            }
        }

        return view
    }

    companion object {
        val TAG = "WebviewFragment"

        fun newInstance(website:String):WebviewFragment{

            val webviewFragment = WebviewFragment()
            val args = Bundle()
            args.putString(AppConstants.REQ_WEBSITE,website)
            webviewFragment.arguments = args
            return webviewFragment
        }
    }

}
